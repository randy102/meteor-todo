import { Meteor } from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base'
import '../imports/api/tasks';


Meteor.startup(() => {
  if (!Accounts.findUserByUsername('meteorite')) {
    Accounts.createUser({
      username: 'meteorite',
      password: 'password'
    });

  }

  if (!Accounts.findUserByUsername('user1')) {
    Accounts.createUser({
      username: 'user1',
      password: 'password'
    });
  }
  
});
