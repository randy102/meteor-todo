import React, { useState } from 'react';
import Task from './Task.jsx';
import { useTracker } from 'meteor/react-meteor-data';
import {Tasks} from '../api/tasks.js';
import { TaskForm } from './TaskForm.jsx';
import _ from 'lodash'
import { LoginForm } from './LoginForm.jsx';
import {Meteor} from 'meteor/meteor'

const toggleChecked = ({ _id, isChecked }) => {
  Meteor.call('tasks.setChecked', _id, Boolean(!isChecked))
};

const deleteTask = ({ _id }) => Meteor.call('tasks.remove', _id);

const togglePrivate = ({ _id, isPrivate }) => {
  Meteor.call('tasks.setPrivate', _id, !isPrivate);
};
 
export function App(){
  const [hideCompleted, setHideCompleted] = useState(false);
  const filter = {}
  if (hideCompleted) {
    _.set(filter, 'isChecked', {$ne: true});
  }

  const { taskList, incompleteTasksCount, user } = useTracker(() => {
    Meteor.subscribe('tasks',Meteor.userId());
 
    return ({
      taskList: Tasks.find(filter, {sort: {createdAt: -1}}).fetch(),
      incompleteTasksCount: Tasks.find({checked: {$ne: true}}).count(),
      user: Meteor.user(),
    });
  });
  
  if (!user) {
    return (
      <div className="simple-todos-react">
        <LoginForm/>
      </div>
    );
  }


  return (
    <div className="simple-todos-react">
      <h1>{user.username}'s Todo List ({ incompleteTasksCount })</h1>

      <div className="filters">
        <label>
          <input
              type="checkbox"
              readOnly
              checked={ Boolean(hideCompleted) }
              onClick={() => setHideCompleted(!hideCompleted)}
          />
          Hide Completed
        </label>
      </div>

      <ul className="tasks">
        {taskList.map(task => (
          <Task 
            key={task._id} 
            task={task} 
            onCheckboxClick={toggleChecked}
            onDeleteClick={deleteTask}
            onTogglePrivateClick={togglePrivate}
          />
        ))}
        
      </ul>

      <TaskForm  user={user}/>
    </div>
  )
}